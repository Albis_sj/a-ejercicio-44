import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    //Si entra a Especialidades, carga el loadChildren
    path: 'especialidades',
    //Cargar las rutas hijas, se cargan mediante el modulo, relacionado a los componentes
    loadChildren: () => import ('./especialidades/especialidades.module').then(m => m.EspecialidadesModule),
  },
  {
    path: 'carreras',
    loadChildren: () => import('./carreras/carreras.module').then(m => m.CarrerasModule),
  },
  {
    path: 'mlaptop',
    loadChildren: () => import ('./m-laptop/m-laptop.module').then(m => m.MLaptopModule),
  },{
    path: 'mgaseosa', 
    loadChildren: () => import('./m-gaseosa/m-gaseosa.module').then(m => m.MGaseosaModule)
  },
  {
    path: '**',
    redirectTo: 'especialidades'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
