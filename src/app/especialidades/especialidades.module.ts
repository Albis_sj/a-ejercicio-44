import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DfrontEndComponent } from './pages/dfront-end/dfront-end.component';
import { DbackEndComponent } from './pages/dback-end/dback-end.component';
import { DfullStackComponent } from './pages/dfull-stack/dfull-stack.component';
import { EspecialidadesRoutingModule } from './especialidades-routing.module';



@NgModule({
  declarations: [
    DfrontEndComponent,
    DbackEndComponent,
    DfullStackComponent
  ],
  imports: [
    CommonModule,
    EspecialidadesRoutingModule
  ]
})
export class EspecialidadesModule { }
