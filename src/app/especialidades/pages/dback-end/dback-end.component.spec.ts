import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DbackEndComponent } from './dback-end.component';

describe('DbackEndComponent', () => {
  let component: DbackEndComponent;
  let fixture: ComponentFixture<DbackEndComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DbackEndComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DbackEndComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
