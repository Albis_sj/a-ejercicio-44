import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { DfrontEndComponent } from './pages/dfront-end/dfront-end.component';
import { DbackEndComponent } from './pages/dback-end/dback-end.component';
import { DfullStackComponent } from './pages/dfull-stack/dfull-stack.component';

const routes: Routes = [
  {
    path: '',
    children: [
      { path: 'dfrontend', component: DfrontEndComponent },
      { path: 'dbackend', component: DbackEndComponent },
      { path: 'dfullstack', component: DfullStackComponent },
      { path: '**', redirectTo: 'dfrontend' },
    ]
  }
];

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forChild(routes)
  ]
})
export class EspecialidadesRoutingModule { }
